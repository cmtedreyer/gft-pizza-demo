//
//  DrinkTableViewCell.swift
//  Pizza
//
//  Created by Tiago N Dreyer on 09/12/18.
//  Copyright © 2018 Tiago N Dreyer. All rights reserved.
//

import UIKit

class DrinkTableViewCell: UITableViewCell {
    @IBOutlet weak var addDrinkButton: UIButton!
    @IBOutlet weak var drinkLabel: UILabel!
    var drinkController: DrinksViewController!
    var drink: Drink!
    
    @IBAction func addDrinkToChart(_ sender: Any) {
        self.drinkController.addDrinkToCart(self.drink)
    }
}
