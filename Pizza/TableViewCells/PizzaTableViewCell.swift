//
//  PizzaTableViewCell.swift
//  Pizza
//
//  Created by Tiago N Dreyer on 07/12/18.
//  Copyright © 2018 Tiago N Dreyer. All rights reserved.
//

import UIKit

class PizzaTableViewCell: UITableViewCell {
    @IBOutlet weak var pizzaNameLabel: UILabel!
    @IBOutlet weak var ingredientsLabel: UILabel!
    @IBOutlet weak var pizzaImage: UIImageView!
    @IBOutlet weak var addToCartButton: UIButton!
    var pizza: Pizza!
    var controller: MainViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let image = UIImage(named: "bg_wood"){
            let color =  UIColor(patternImage: image)
            self.pizzaImage.backgroundColor = color
        }
        self.addToCartButton.layer.cornerRadius = 5.0
    }
    
    @IBAction func addToCart(_ sender: Any) {
        self.controller.prepareToAddPizzaToCart(self.pizza)
    }
}
