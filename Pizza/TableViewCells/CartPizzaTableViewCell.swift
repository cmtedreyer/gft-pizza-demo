//
//  CartPizzaTableViewCell.swift
//  Pizza
//
//  Created by Tiago N Dreyer on 07/12/18.
//  Copyright © 2018 Tiago N Dreyer. All rights reserved.
//

import UIKit

class CartPizzaTableViewCell: UITableViewCell {
    @IBOutlet weak var pizaLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
}
