//
//  DrinksViewController.swift
//  Pizza
//
//  Created by Tiago N Dreyer on 07/12/18.
//  Copyright © 2018 Tiago N Dreyer. All rights reserved.
//

import UIKit

class DrinksViewController: UIViewController {
    @IBOutlet weak var drinkTableView: UITableView!
    @IBOutlet weak var addToCartView: UIView!
    @IBOutlet weak var addedToCartLabel: UILabel!    
    @IBOutlet weak var drinksTableView: UITableView!
    //Class variables
    var drinks: [Drink] = []
    var mainController: MainViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "DRINKS"
        if let drk = Drink.fetchAll(self.mainController.context) {
            self.drinks = drk
        }
        self.drinksTableView.reloadData()
    }
    
    func addDrinkToCart(_ drink: Drink) {
        /*
         This function should be called by tableview cell´s add button tap. It adds the selected drink to the Cart object
        */
        DispatchQueue.global(qos: .userInteractive).async {
            DispatchQueue.main.async {
                self.addedToCartLabel.text = "\(drink.name ?? "Your Drink") was added to cart!"
                self.addToCartView.isHidden = false
            }
            let item = CartItem(context: self.mainController.context)
            item.drink = drink
            item.price = drink.price
            self.mainController.cart.addToItems(item)
            self.mainController.saveContext()
            sleep(3)
            DispatchQueue.main.async {
                self.addToCartView.isHidden = true
            }
        }
    }
}

extension DrinksViewController: UITableViewDelegate, UITableViewDataSource {
    /*
     This extension contains all tableview´s methods. It´s separeted from the Class to make the code cleaner
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.drinks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DrinkTableViewCell", for: indexPath) as! DrinkTableViewCell
        let drink = self.drinks[indexPath.row]
        cell.drinkLabel.text = drink.name
        cell.drink = drink
        cell.drinkController = self
        return cell
    }
}
