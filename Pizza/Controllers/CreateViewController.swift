//
//  CreateViewController.swift
//  Pizza
//
//  Created by Tiago N Dreyer on 07/12/18.
//  Copyright © 2018 Tiago N Dreyer. All rights reserved.
//

import UIKit
import CoreData

class CreateViewController: UIViewController {
    @IBOutlet weak var pizzaImage: UIImageView!
    var mainController: MainViewController!
    @IBOutlet weak var ingredientsTableView: UITableView!
    var ingredients: [Ingredient] = []
    var selectedIngredients: [Ingredient] = []
    var pizza: Pizza?
    var isCustomizingBeforeAddToCart = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let pz = self.pizza {
            self.selectedIngredients = pz.ingredients?.array as! [Ingredient]
            if let imageData = pz.image {
                if let image = UIImage(data: imageData) {
                    self.pizzaImage.image = image
                }
            }
            if let image = UIImage(named: "bg_wood"){
                let color =  UIColor(patternImage: image)
                self.pizzaImage.backgroundColor = color
            }
        }
        if let ing = Ingredient.fetchAll(self.mainController.context) {
            self.ingredients = ing
            self.ingredientsTableView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);
        if self.isMovingFromParent {
            if !self.selectedIngredients.isEmpty {
                if isCustomizingBeforeAddToCart {
                    if let pizza = self.pizza {
                        let newPizza = Pizza(context: self.mainController.context)
                        newPizza.basePrice = pizza.basePrice
                        newPizza.image = pizza.image
                        newPizza.name = "Custom \(pizza.name!)"
                        for ingredient in self.selectedIngredients {
                            if !newPizza.ingredients!.contains(ingredient) {
                                newPizza.addToIngredients(ingredient)
                            }
                        }
                        self.mainController.addPizzaToCart(newPizza)
                    }
                } else {
                    let newPizza = Pizza(context: self.mainController.context)
                    newPizza.basePrice = 4.0
                    var pizzaNumber = 1
                    if let savedPizzas = Pizza.fetchAll(self.mainController.context) {
                        pizzaNumber = savedPizzas.filter({$0.isCustom == true}).count + 1
                    }
                    newPizza.isCustom = true
                    newPizza.name = "Custom Pizza #" + String(pizzaNumber)
                    for ingredient in self.selectedIngredients {
                        newPizza.addToIngredients(ingredient)
                    }
                    do {
                        try self.mainController.context.save()
                    } catch let error as NSError {
                        print("Could not save. \(error), \(error.userInfo)")
                    }
                }
            }
            self.mainController.loadMenu()
        }
    }
}

extension CreateViewController: UITableViewDelegate, UITableViewDataSource {
    /*
     This extension contains all tableview´s methods. It´s separeted from the Class to make the code cleaner
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ingredients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IngredientTableViewCell", for: indexPath) as! IngredientTableViewCell
        let ingredient = self.ingredients[indexPath.row]
        cell.ingredientLabel.text = ingredient.name
        cell.ingredient = ingredient
        if self.selectedIngredients.contains(ingredient) {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            cell.accessoryType = .checkmark
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? IngredientTableViewCell {
            if let index = self.selectedIngredients.index(of: cell.ingredient) {
                self.selectedIngredients.remove(at: index)
            }
            cell.accessoryType = .none
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? IngredientTableViewCell {
            self.selectedIngredients.append(cell.ingredient)
            cell.accessoryType = .checkmark
        }
    }
}
