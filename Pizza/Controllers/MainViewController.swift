//
//  ViewController.swift
//  Pizza
//
//  Created by Tiago N Dreyer on 07/12/18.
//  Copyright © 2018 Tiago N Dreyer. All rights reserved.
//
import UIKit
import CoreData
class MainViewController: UIViewController {
    @IBOutlet weak var pizzaTableView: UITableView!
    @IBOutlet weak var addedToCartView: UIView!
    @IBOutlet weak var addToCarLabel: UILabel!
    let httpHandler = HttpHandler()
    var productSync: ProductSync!
    var context: NSManagedObjectContext!
    var pizzas: [Pizza] = []
    var cart: Cart!
    var isMenuUpdated = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.context = appDelegate.persistentContainer.viewContext
        self.productSync = ProductSync(mainController: self)
        if let cart = Cart.getActive(self.context) {
            self.cart = cart
        } else {
            self.cart = Cart(context: self.context)
            self.cart.proccessed = false
        }
        self.updateMenu()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "createSegue" {
            navigationItem.title = ""
            let destination = segue.destination as! CreateViewController
            destination.mainController = self
        } else if segue.identifier == "cartSegue" {
            navigationItem.title = ""
            let destination = segue.destination as! CartViewController
            destination.mainController = self
        }        
    }
    
    func updateMenu() {
        /*
         This method is used to retrieve menu items from the remote API. In case of error (such as no internet access), an alert should be displayed.
        */
        DispatchQueue.global().async {
            self.productSync.syncRemteData(completionHandler: { success in
                self.saveContext()
                DispatchQueue.main.async {
                    if success == false {
                        let message = "Impossible to sync our menu. Please check your Internet connection."
                        let alert = UIAlertController(title: "Warning", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true)
                    }
                    self.loadMenu()
                }
            })
        }
    }
    
    func loadMenu() {
        /*
         Simple method to fetch Pizza objects and update the tableView
        */
        if let pzs = Pizza.fetchAll(self.context) {
            self.pizzas = pzs
            self.pizzaTableView.reloadData()
        }
    }
    
    func prepareToAddPizzaToCart(_ pizza: Pizza) {
        /*
         This method is called by taping the "Add to cart" button on a tableViewCell. It asks if the user wish to customize the pizza ingredients and procceeds as chosen.
        */
        let message = "Do you want to select your ingredients before adding this pizza to your cart?"
        let alert = UIAlertController(title: "Warning", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let createViewController = storyboard.instantiateViewController(withIdentifier: "CreateViewController") as! CreateViewController
            createViewController.mainController = self
            createViewController.pizza = pizza
            createViewController.isCustomizingBeforeAddToCart = true
            createViewController.navigationItem.title = ""
            self.navigationController?.pushViewController(createViewController, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { (alert) in
            self.addPizzaToCart(pizza)
        }))
        self.present(alert, animated: true)
    }
    
    func addPizzaToCart(_ pizza: Pizza) {
        /*
         This method is used to add a pizza to the Cart object and calculate it´s final price.
        */
        DispatchQueue.global(qos: .userInteractive).async {
            DispatchQueue.main.async {
                self.addToCarLabel.text = "\(pizza.name ?? "Your Pizza") was added to cart!"
                self.addedToCartView.isHidden = false
            }
            let item = CartItem(context: self.context)
            item.pizza = pizza
            item.price = (pizza.ingredients!.array as! [Ingredient]).reduce(0, {$0 + $1.price}) + pizza.basePrice
            self.cart.addToItems(item)
            self.saveContext()
            sleep(3)
            DispatchQueue.main.async {
                self.addedToCartView.isHidden = true
            }
        }
    }
    
    func saveContext() {
        /*
         Simple method to save NSManagedObjectContext data
        */
        do {
            try self.context.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
}


extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    /*
     This extension contains all tableview´s methods. It´s separeted from the Class to make the code cleaner
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pizzas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PizzaTableViewCell", for: indexPath) as! PizzaTableViewCell
        let pizza = self.pizzas[indexPath.row]
        cell.pizza = pizza
        cell.controller = self
        if let imageData = pizza.image {
            if let image = UIImage(data: imageData) {
                cell.pizzaImage.image = image
            }
        } else {
            cell.pizzaImage.image = UIImage(named: "default_pizza")
        }
        var ingredientText = ""
        if let ingredientList = pizza.ingredients?.array as? [Ingredient] {
            for ingredient in ingredientList {
                if ingredientText.isEmpty {
                    ingredientText += ingredient.name!
                } else {
                    ingredientText += ", " + ingredient.name!
                }
            }
        }
        let price = (pizza.ingredients!.array as! [Ingredient]).reduce(0, {$0 + $1.price}) + pizza.basePrice
        cell.addToCartButton.setTitle("$ \(String(price))", for: .normal)
        cell.ingredientsLabel.text = ingredientText
        cell.pizzaNameLabel.text = pizza.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let pizza = self.pizzas[indexPath.row]
        return pizza.isCustom
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let pizza = self.pizzas[indexPath.row]
            self.context.delete(pizza)
            self.saveContext()
            self.pizzas.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
        }
    }
}

