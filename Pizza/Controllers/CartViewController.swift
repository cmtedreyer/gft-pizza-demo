//
//  CartViewController.swift
//  Pizza
//
//  Created by Tiago N Dreyer on 07/12/18.
//  Copyright © 2018 Tiago N Dreyer. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {
    @IBOutlet weak var cehckoutButton: UIButton!
    @IBOutlet weak var pizzaTableView: UITableView!
    var mainController: MainViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "CART"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.pizzaTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "drinkSegue" {
            navigationItem.title = ""
            let destination = segue.destination as! DrinksViewController
            destination.mainController = self.mainController
        }
    }
    
    @IBAction func checkOut(_ sender: Any) {
        /*
         This method will be called when tapping "CHCKOUT" button
        */
        if let chackoutResponse = self.mainController.httpHandler.postCheckout(cart: self.mainController.cart) {
            //REMARK: The HTTP SERVER wasn't working during my tests/implementation.
            print(chackoutResponse)
        } else {
            //REMARK: I SHOULD HANDLE HTTP POST ERRORS HERE, BUT I'M FAKING A SUCCESSFULL POST, DUE TO THE SERVER AVAILABILITY
            self.terminateCheckout(cart: self.mainController.cart)
        }
    }
    
    func terminateCheckout(cart: Cart) {
        /*
         This method persists changes on the Cart object
        */
        let message = "Thank you for your order. It is being proccessed and will be delivered soon!"
        let alert = UIAlertController(title: "Thank You!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            cart.proccessed = true
            self.mainController.saveContext()
            self.mainController.cart = Cart(context: self.mainController.context)
            _ = self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true)
    }
}

extension CartViewController: UITableViewDataSource, UITableViewDelegate {
    /*
     This extension contains all tableview´s methods. It´s separeted from the Class to make the code cleaner
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mainController.cart.items!.allObjects.count + 1
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < self.mainController.cart.items!.allObjects.count {
            return 44.0
        } else {
            return 54.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartPizzaTableViewCell", for: indexPath) as! CartPizzaTableViewCell
        if indexPath.row < self.mainController.cart.items!.allObjects.count {
            let cartItem = self.mainController.cart.items!.allObjects[indexPath.row] as! CartItem
            cell.priceLabel.text = "$ \(String(cartItem.price))"
            if let pizza = cartItem.pizza {
                cell.pizaLabel.text = pizza.name
            } else if let drink = cartItem.drink {
                cell.pizaLabel.text = drink.name
            } else {
                cell.pizaLabel.text = ""
            }
            cell.pizaLabel.font = UIFont.systemFont(ofSize: 17.0)
            cell.priceLabel.font = UIFont.systemFont(ofSize: 17.0)
        } else {
            cell.pizaLabel.text = "TOTAL"
            cell.pizaLabel.font = UIFont.boldSystemFont(ofSize: 19.0)
            cell.priceLabel.text = "$ \((self.mainController.cart.items!.allObjects as! [CartItem]).reduce(0, {$0 + $1.price}))"
            cell.priceLabel.font = UIFont.boldSystemFont(ofSize: 19.0)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row < self.mainController.cart.items!.allObjects.count {
        return true
        } else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let item = self.mainController.cart.items!.allObjects[indexPath.row] as! CartItem
            self.mainController.cart.removeFromItems(item)
            self.mainController.saveContext()
            tableView.reloadData()
        }
    }
}
