//
//  Pizza.swift
//  Pizza
//
//  Created by Tiago N Dreyer on 07/12/18.
//  Copyright © 2018 Tiago N Dreyer. All rights reserved.
//

import Foundation
import CoreData

extension Pizza {
    class func fetchAll(_ context: NSManagedObjectContext) -> [Pizza]? {
        /*
         Simple class function to return an array of objects in a simpler way
         */
        let request: NSFetchRequest<Pizza> = Pizza.fetchRequest()
        do {
            let pizzas = try context.fetch(request)
            return pizzas
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    class func instance(_ context: NSManagedObjectContext,  with name: String) -> Pizza? {
        /*
         Simple class function to return asingle object by an property, in a simpler way
         */
        
        let request: NSFetchRequest<Pizza> = Pizza.fetchRequest()
        let predicate = NSPredicate(format: "name = %@", argumentArray: [name])
        request.predicate = predicate
        do {
            let pizzas = try context.fetch(request)
            return pizzas.first
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
}
