//
//  Cart.swift
//  Pizza
//
//  Created by Tiago N Dreyer on 07/12/18.
//  Copyright © 2018 Tiago N Dreyer. All rights reserved.
//

import Foundation
import CoreData

extension Cart {
    class func getActive(_ context: NSManagedObjectContext) -> Cart? {
        /*
         Simple class function to return asingle object by an property, in a simpler way
         */
        let request: NSFetchRequest<Cart> = Cart.fetchRequest()
        let predicate = NSPredicate(format: "proccessed = %@", argumentArray: [false])
        request.predicate = predicate
        do {
            let carts = try context.fetch(request)
            return carts.first
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
}
