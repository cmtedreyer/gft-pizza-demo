//
//  Ingredient.swift
//  Pizza
//
//  Created by Tiago N Dreyer on 07/12/18.
//  Copyright © 2018 Tiago N Dreyer. All rights reserved.
//

import Foundation
import CoreData

extension Ingredient {
    class func fetchAll(_ context: NSManagedObjectContext) -> [Ingredient]? {
        /*
         Simple class function to return an array of objects in a simpler way
        */
        let request: NSFetchRequest<Ingredient> = Ingredient.fetchRequest()
        do {
            let ingredients = try context.fetch(request)
            return ingredients
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    class func instance(_ context: NSManagedObjectContext,  with id: Int32) -> Ingredient? {
        /*
         Simple class function to return asingle object by an property, in a simpler way
         */
        let request: NSFetchRequest<Ingredient> = Ingredient.fetchRequest()
        let predicate = NSPredicate(format: "id = %@", argumentArray: [id])
        request.predicate = predicate
        
        do {
            let ingredients = try context.fetch(request)
            return ingredients.first
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
}
