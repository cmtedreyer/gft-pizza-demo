//
//  RemoteFetcher.swift
//  Pizza
//
//  Created by Tiago N Dreyer on 07/12/18.
//  Copyright © 2018 Tiago N Dreyer. All rights reserved.
//

import Foundation
import Alamofire
import Alamofire_Synchronous

class HttpHandler {
    /*
     Simple class to handle sample API data FETCH
     */
    
    init() {
    }
    
    struct Urls {
        static let ingredients = "https://api.myjson.com/bins/ozt3z"
        static let drinks = "https://api.myjson.com/bins/150da7"
        static let pizzas = "https://api.myjson.com/bins/dokm7"
        static let checkout = "http://posttestserver.com/post.php"
    }
    
    func fetchRemoteIngredients() -> [NSDictionary]? {
        /*
         Fetches remote JSON, returning it as NSDictionary
         */
        return Alamofire.request(Urls.ingredients).responseJSON(options: .allowFragments).result.value as? [NSDictionary]
    }
    
    func fetchRemoteDrinks() -> [NSDictionary]? {
        /*
         Fetches remote JSON, returning it as NSDictionary
         */
        return Alamofire.request(Urls.drinks).responseJSON(options: .allowFragments).result.value as? [NSDictionary]
    }
    
    func fetchRemotePizzas() -> NSDictionary? {
        /*
         Fetches remote JSON, returning it as NSDictionary
         */
        return Alamofire.request(Urls.pizzas).responseJSON(options: .allowFragments).result.value as? NSDictionary
    }
    
    func prepareAwarenessDelivery(_ cart: Cart) -> [String : Any?] {
        /*
         Simple method to prepare JSON data, based on a Cart object, to be posted
         */
        let items = cart.items!.allObjects as! [CartItem]
        var pizzas:  [[String : Any?]] = []
        var drinks: [Int32] = []
        for item in items {
            if let pizza = item.pizza {
                var ingredients : [Int32] = []
                for ingredient in pizza.ingredients!.array as! [Ingredient] {
                    ingredients.append(ingredient.id)
                }
                let pizzaDict: [String : Any?] = ["name" : pizza.name!,
                                                  "ingredients" : ingredients
                ]
                pizzas.append(pizzaDict)
            } else if let drink = item.drink {
                drinks.append(drink.id)
            }
        }
        let httpBody: [String : Any?] = ["pizzas" : pizzas,
                                         "drinks" : drinks
        ]
        return httpBody
    }
    
    
    func postCheckout(cart: Cart) -> NSDictionary? {
        /*
         This function uses Alamofire to POST cart´s items to the remote API URL
         */
        // REMARKS: THE POST URL WASN´T WORKING DURING MY TESTS, SO I COULDN´T BE SURE IF THIS FUNCTION IS WORKING.
        let dic = self.prepareAwarenessDelivery(cart)
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            var request = URLRequest(url: URL(string: Urls.checkout)!)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonData
            if let response = Alamofire.request(request).responseJSON(options: .allowFragments).result.value as? NSDictionary {
                return response
            } else {
                return nil
            }
        } catch {
            print("Error serializing json: \(error.localizedDescription)")
            return nil
        }
    }
}
