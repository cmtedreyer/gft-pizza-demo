//
//  File.swift
//  Pizza
//
//  Created by Tiago N Dreyer on 07/12/18.
//  Copyright © 2018 Tiago N Dreyer. All rights reserved.
//

import UIKit
import CoreData

class ProductSync {
    var mainController: MainViewController
    
    init(mainController: MainViewController) {
        self.mainController = mainController
    }    
    
    func syncRemteData(completionHandler: (_ success: Bool) -> Void) {
        /*
         Simple method to call the other methods in this class, in sequence. It should run the completion handler after calling all methods
        */
        var success = true
        if self.syncIngredients() == false {
            success = false
        }
        if self.syncPizzas() == false {
            success = false
        }
        if self.syncDrinks() == false {
            success = false
        }
        completionHandler(success)
        
    }
    
    func syncIngredients() -> Bool {
        /*
         This method fetches remote ingredients from API, creates Ingredient objects, to be stored using CoreData.
         It returns a Boolean, depending on the API return, allowing a simple error handling.
        */
        if let ingredients = self.mainController.httpHandler.fetchRemoteIngredients() {
            for ingredient in ingredients {
                let id = ingredient["id"] as! Int32
                let name = ingredient["name"] as! String
                let price = ingredient["price"] as! Double
                var ingredientObj: Ingredient
                if let existent = Ingredient.instance(self.mainController.context, with: id) {
                    ingredientObj = existent
                } else {
                    ingredientObj  = Ingredient(context: self.mainController.context)
                    ingredientObj.id = id
                }
                ingredientObj.name = name
                ingredientObj.price = price
            }
            self.mainController.saveContext()
            return true
        } else {
            return false
        }
    }
    
    func syncPizzas() -> Bool {
        /*
        This method fetches remote pizzas from API, creates Pizza objects, to be stored using CoreData.
        It returns a Boolean, depending on the API return, allowing a simple error handling.
        */
        if let pizaData = self.mainController.httpHandler.fetchRemotePizzas() {
            let basePrice = pizaData["basePrice"] as! Double
            for pizza in pizaData["pizzas"] as! [NSDictionary] {
                let name =  pizza["name"] as! String
                let ingredients = pizza["ingredients"] as! [Int32]
                var pizzaObject: Pizza
                if let existent = Pizza.instance(self.mainController.context, with: name) {
                    pizzaObject = existent
                } else {
                    pizzaObject = Pizza(context: self.mainController.context)
                    if let imageUrl = pizza["imageUrl"] as? String {
                        if let url = URL(string: imageUrl) {
                            do {
                                let imageData = try Data(contentsOf: url)
                                pizzaObject.image = imageData
                            } catch {
                                print("Unable to get pizza´s image: \(error)")
                            }
                        }
                    }
                }
                pizzaObject.name = name
                pizzaObject.basePrice = basePrice
                for ingredientId in ingredients {
                    if let ingredient = Ingredient.instance(self.mainController.context, with: ingredientId) {
                        if pizzaObject.ingredients == nil || (pizzaObject.ingredients?.contains(ingredient) != true) {
                            pizzaObject.addToIngredients(ingredient)
                        }
                    }
                }
            }
            return true
        } else {
            return false
        }
    }
    
    func syncDrinks() -> Bool {
        /*
        This method fetches remote drinks from API, creates Drink objects, to be stored using CoreData.
        It returns a Boolean, depending on the API return, allowing a simple error handling.
        */
        if let drinks = self.mainController.httpHandler.fetchRemoteDrinks() {
            for drink in drinks {
                let id = drink["id"] as! Int32
                let name = drink["name"] as! String
                let price = drink["price"] as! Double
                
                let drinkObject: Drink
                if let dk = Drink.instance(self.mainController.context, with: id) {
                    drinkObject = dk
                } else {
                    drinkObject = Drink(context: self.mainController.context)
                }
                drinkObject.id = id
                drinkObject.name = name
                drinkObject.price = price
            }
            return true
        } else {
            return false
        }
    }
}
